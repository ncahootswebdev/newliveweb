<?php
if (!class_exists('ARM_Admin_mycred_feature'))
{
    class ARM_Admin_mycred_feature 
    {
        var $ismyCREDFeature;
        function __construct()
        {
            global $wpdb, $ARMember;
            $arm_admin_mycred_feature = get_option('arm_is_mycred_feature');
            $this->ismyCREDFeature = ($arm_admin_mycred_feature == '1') ? true : false;
            add_action('arm_add_new_custom_add_on', array(&$this, 'arm_add_mycred_addon'));
            add_action('mycred_deactivation',array(&$this, 'arm_mycred_deactivation'));
        }
        function arm_add_mycred_addon()
        {
            global $arm_members_activity, $armemberplugin;
            $setact = 0;
            $setact = $arm_members_activity->$armemberplugin();
            $featureActiveIcon = MEMBERSHIP_IMAGES_URL . '/feature_active_icon.png';
                ?>     <div class="arm_feature_list mycred_enable <?php echo ($this->ismyCREDFeature == true) ? 'active':'';?>">
                    <div class="arm_feature_active_icon"><img src="<?php echo $featureActiveIcon;?>"  alt="active icon" width="100%"/></div>
                    <div class="arm_feature_content">
                        <div class="arm_feature_title"><?php _e('myCRED Integration','ARMember'); ?></div>
                        <div class="arm_feature_text"><?php _e("Integrate myCRED adaptive points management system with ARMember.", 'ARMember');?></div>
                        <?php if ($setact != 1) { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($this->ismyCREDFeature == 1) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_activation_license" data-feature_val="1" data-feature="social"><?php _e('Activate','ARMember'); ?></a>
                            <img src="<?php echo MEMBERSHIP_IMAGES_URL.'/arm_loader.gif' ?>" class="arm_addon_loader_img" width="24" height="24" />
                        </div>
                        <?php } else { ?>
                        <div class="arm_feature_button_activate_wrapper <?php echo ($this->ismyCREDFeature == true) ? 'hidden_section':'';?>">
                            <a href="javascript:void(0)" class="arm_feature_activate_btn arm_feature_settings_switch" data-feature_val="1" data-feature="mycred"><?php _e('Activate','ARMember'); ?></a>
                            <img src="<?php echo MEMBERSHIP_IMAGES_URL.'/arm_loader.gif' ?>" class="arm_addon_loader_img" width="24" height="24" />
                        </div>
                        <?php } ?>
                        <div class="arm_feature_button_deactivate_wrapper <?php echo ($this->ismyCREDFeature == true) ? '':'hidden_section';?>">
                            <a href="javascript:void(0)" class="arm_feature_deactivate_btn arm_feature_settings_switch" data-feature_val="0" data-feature="mycred"><?php _e('Deactivate','ARMember'); ?></a>
                            <img src="<?php echo MEMBERSHIP_IMAGES_URL.'/arm_loader.gif' ?>" class="arm_addon_loader_img" width="24" height="24" />
                        </div>
                    </div>
                    <a class="arm_ref_info_links arm_feature_link arm_advanced_link" target="_blank" href="https://www.armemberplugin.com/documents/mycred-support/"><?php _e('More Info', 'ARMember'); ?></a>
                </div> <?php
        }
        function arm_mycred_deactivation()
        {
            update_option('arm_is_mycred_feature', 0);
        }
    }
}
global $arm_admin_mycred_feature;
$arm_admin_mycred_feature = new ARM_Admin_mycred_feature();
$arm_is_mycred_active = get_option('arm_is_mycred_feature');
if(!empty($arm_is_mycred_active) && $arm_is_mycred_active==1)
{
    add_filter('mycred_setup_hooks','arm_mycred_hook');
    function arm_mycred_hook($arm_mycred_installed)
    {
        $arm_mycred_installed['arm_mycred'] = array(
            'title' => __('ARMember Membership', 'ARMember'),
            'description' => __('ARMember Premium Plugin - Buy Membership Plan Hook', 'ARMember'),
            'callback' => array('ARM_mycred_feature')
            );
        return $arm_mycred_installed;
    }
    add_action('mycred_load_hooks','arm_mycred_custom_hook');
    function arm_mycred_custom_hook()
    {
        if(file_exists(MEMBERSHIP_CLASSES_DIR . "/class.arm_mycred_feature.php")){
            require_once( MEMBERSHIP_CLASSES_DIR . "/class.arm_mycred_feature.php");
        }
    }
}