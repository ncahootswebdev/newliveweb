<?php 
add_action( 'init', 'tte_kingcomposer_extend' );
	function tte_kingcomposer_extend() {

global $kc;
    $kc->add_map(
        array(
            'element_id' => array(
                'name'        => __( 'Text Title', 'domain' ),
                'description' => __( 'Display text and description', 'domain' ),
                'category'    => __( 'TTE-Studio', 'domain' ),
                'icon'        => 'fa-header',
                'params'      => array(
                    array(
                        'name'        => 'title',
                        'label'       => __( 'Title', 'domain' ),
                        'type'        => 'text',
                        'description' => __( 'Insert text title', 'domain' ),
                        'value'       => 'Insert Title',
                        'admin_label' => true
                    ),
                    array(
                        'name'        => 'desc',
                        'label'       => __( 'Description', 'domain' ),
                        'type'        => 'editor',
                        'description' => __( 'Text description', 'domain' ),
                        'value'       => base64_encode( 'Insert Content' )
                    ),
                )
            )
        )
    );

	}