<?php 
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/*Slider*/
/*Who we are*/
Container::make( 'post_meta', 'Слайдер' )
   	 ->show_on_post_type('slider')
	 ->add_fields( array(
			Field::make( 'complex', 'sdr_content', 'Слайдер' )
				->add_fields( array(
				Field::make( 'text', 'slider_title', '	Заголовок Слайда' )
						 ->set_width( 100 ),
				Field::make( 'Rich text', 'slider_content', 'Текст Слайдера' )
						 ->set_width( 100 ),
				 )
				)
	) );
/*End Who we are*/
/*Services*/
Container::make( 'post_meta', 'Секция Наши услуги' )
	 ->show_on_post_type('page') // этот метод можно не писать, так как show_on_post_type('post') по умолчанию
	 ->show_on_template('page-services.php')	 
		->add_fields( array(
			Field::make( 'complex', 'our_services', 'Список наших услуг' )
				->add_fields( array(
				Field::make( 'image', 'service_background', 'Картинка Услуги ' )->set_width( 30 ),
				Field::make( 'textarea', 'service_textarea_short', 'Описание Услуги краткое' )->set_width( 70 ),
				Field::make( 'textarea', 'service_textarea', 'Описание Услуги ' )->set_width( 100 ),
				 )
			)
	) );
	
Container::make( 'post_meta', 'Секция - Доставка' )
	 ->show_on_post_type('page') // этот метод можно не писать, так как show_on_post_type('post') по умолчанию
     ->show_on_template('page-services.php')		
		 ->add_fields(array(
			  Field::make( 'textarea', 'section_delivery_title', 'Заголовок Секции Доставка' )->set_width( 100 ),
			  Field::make( 'image', 'section_delivery_background', 'Секции Доставка' )->set_value_type( 'url' )->set_width( 30 ),
			  Field::make( 'textarea', 'section_delivery_textarea', 'Описание Секции Доставка' )->set_width( 70 ),
		 ));
/*End Services*/
/*Laboratory*/
Container::make( 'post_meta', 'Секция 2, информация о Лаборатории' )
	 ->show_on_post_type('page') // этот метод можно не писать, так как show_on_post_type('post') по умолчанию
	 ->show_on_template('page-laboratory.php')	 
		 ->add_fields(array(
			 Field::make('textarea', 'lab_description', 'Текст о Лаборатории')
		 ));

Container::make( 'post_meta', 'Секция 3, Лаборатория проверяет' )
	 ->show_on_post_type('page') // этот метод можно не писать, так как show_on_post_type('post') по умолчанию
     ->show_on_template('page-laboratory.php')		
		 ->add_fields(array(
			 Field::make('textarea', 'crb_laboratory_check_param', 'Заголовок, Лаборатория проверяет'),
			 Field::make('textarea', 'crb_laboratory_param1', 'Параметр 1, Название'),
			 /*ISO1*/
			 Field::make( 'Rich text', 'iso1_textarea', 'Параметр 1, Блок ISO1' )->set_width( 100 ),
			  /*ISO2*/
			 Field::make( 'Rich text', 'iso2_textarea', 'Параметр 1, Блок ISO2' )->set_width( 100 ),
			 /*ISO3*/
			 Field::make( 'Rich text', 'iso3_textarea', 'Параметр 1, Блок ISO3' )->set_width( 100 ),
			 /*ISO4*/
			 Field::make( 'Rich text', 'iso4_textarea', 'Параметр 1, Блок ISO4' )->set_width( 100 ),
			 /*ISO5*/
			 Field::make( 'Rich text', 'iso5_textarea', 'Параметр 1, Блок ISO5' )->set_width( 100 ),
			 /*ISO6*/
			 Field::make( 'Rich text', 'iso6_textarea', 'Параметр 1, Блок ISO6' )->set_width( 100 ),
			 Field::make('textarea', 'crb_laboratory_param2_title', 'Параметр 2, Название'),
			 Field::make('textarea', 'crb_laboratory_param2_description', 'Параметр 2, Описание'),
			 Field::make('textarea', 'crb_laboratory_param3_title', 'Параметр 3, Название'),
			 Field::make('textarea', 'crb_laboratory_param3_description', 'Параметр 3, Описание'),
		 ));
Container::make( 'post_meta', 'Секция 4, Подбор цвета и колеровку краски' )
	 ->show_on_post_type('page') // этот метод можно не писать, так как show_on_post_type('post') по умолчанию
	 ->show_on_template('page-laboratory.php')	
 		 ->add_fields(array(
			 Field::make('textarea', 'crb_section4_title', 'Название Секции'),
			 Field::make('textarea', 'crb_section4_description', 'Описание Секции')->set_width( 70 ),
			 Field::make( 'image', 'crb_section4_background', 'фоновая картинка Секции' )->set_value_type( 'url' )->set_width( 30 ),
			 
		 ));
Container::make( 'post_meta', 'Секция 5, Как работает спектрофтометр' )
	 ->show_on_post_type('page') // этот метод можно не писать, так как show_on_post_type('post') по умолчанию
	 ->show_on_template('page-laboratory.php')	
 		 ->add_fields(array(
			 Field::make('textarea', 'crb_section5_title', 'Название Секции'),
			 Field::make('Rich Text', 'crb_section5_description', 'Описание Секции')->set_width( 70 ),
			 Field::make( 'image', 'crb_section5_background', 'фоновая картинка Секции' )->set_value_type( 'url' )->set_width( 30 ),
			 
		 ));
Container::make( 'post_meta', 'Секция 5, Информационные блоки' )
	 ->show_on_post_type('page') // этот метод можно не писать, так как show_on_post_type('post') по умолчанию
	 ->show_on_template('page-laboratory.php')	
 		 ->add_fields(array(
		     /*Block1*/
			 Field::make('textarea', 'crb_section5_block1_description', 'Описание Блока 1 Секции Информационные блоки')->set_width( 70 ),
			 Field::make('textarea', 'crb_section5_block1_add_description', 'Расширенное описание Блока 1 Секции Информационные блоки')->set_width( 70 ),
			 Field::make( 'image', 'crb_section5_block1_background', 'фоновая картинка Блока 1 Секции Информационные блоки' )->set_value_type( 'url' )->set_width( 30 ),
             /*Block1*/
             Field::make('textarea', 'crb_section5_block2_description', 'Описание Блока 2 Секции Информационные блоки')->set_width( 70 ),
			 Field::make('textarea', 'crb_section5_block2_add_description', 'Расширенное описание Блока 2 Секции Информационные блоки')->set_width( 70 ),
			 Field::make( 'image', 'crb_section5_block2_background', 'фоновая картинка Блока 2 Секции Информационные блоки' )->set_value_type( 'url' )->set_width( 30 ),
             /*Block1*/
             Field::make('textarea', 'crb_section5_block3_description', 'Описание Блока 3 Секции Информационные блоки')->set_width( 70 ),
			 Field::make('textarea', 'crb_section5_block3_add_description', 'Расширенное описание Блока 3 Секции Информационные блоки')->set_width( 70 ),
			 Field::make( 'image', 'crb_section5_block3_background', 'фоновая картинка Блока 3 Секции Информационные блоки' )->set_value_type( 'url' )->set_width( 30 ),
			 
		 ));		 
/*End Laboratory*/
/*Clients*/
Container::make( 'post_meta', 'сайт клиента' )
	 ->show_on_post_type('clients') // этот метод можно не писать, так как show_on_post_type('post') по умолчанию
		 ->add_fields(array(
			  Field::make( 'text', 'client_site_url', 'Адрес сайта клиента' )->set_width( 100 ),
		 ));
	 